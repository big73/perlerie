use strict;
use warnings;
use XML::Twig;

my $file = "acomparer.xml";

my  $twig = new XML::Twig;
$twig -> parsefile($file);
$twig -> set_pretty_print('indented');

my $xml_root = $twig -> root;
my $xml_brand = $xml_root -> first_child('brand');
my @tab;


print "-----------------------------------------------------------------------------------xml_root\n";
print $xml_root -> toString."\n";
print "-----------------------------------------------------------------------------------xml_brand\n";
print $xml_brand -> toString."\n";
print "-----------------------------------------------------------------------------------xml_make\n";
my $xml_make = $xml_brand -> first_child('make');
print $xml_make -> toString . "\n";
print "-----------------------------------------------------------------------------------xml_veh\n";
my $xml_veh = $xml_make -> last_child('veh');
print $xml_veh -> toString . "\n";

print "-----------------------------------------------------------------------------------while\n";

do
{
	my $make_name = $xml_make -> att('n');
	print $make_name . "\n";
}
while ($xml_make = $xml_make -> sibling(1))
