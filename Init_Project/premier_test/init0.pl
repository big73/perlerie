use strict;
use warnings;
use XML::Twig;
use Switch;



my $reference_file;
my $acomparer_file;
my $output_file ;

#	Contr�le des param�tres
my $nbparam = $#ARGV;
if ($nbparam == 2)
{
	$reference_file = shift;	
	$acomparer_file = shift;
	$output_file = shift;
}
else
{
	die "Nombre incorrect de param�tres \n 
	
	Utiliser la syntaxe suivante : \n 
	perl $0 <fichier_reference.xml> <fichier_a_comparer.xml> <fichier_de_sortie.xml> \n";
}

#	d�claration des variables n�cessaires au d�marrage du script.
my $twig_reference;							my $twig_acomparer;							my $twig_output;				my $twig_output_initiator;
my $twig_reference_root;					my $twig_acomparer_root;					my $twig_output_root_new;
my @tab_veh_valeur;																		my $twig_output_brand;
																
																						
																						my $twig_output_veh;
																	
											my $acomparer_veh_valeur;											
																						my $twig_output_new;
																						my $twig_output_modified;
																						my $twig_output_deleted;
																						
#	initialisation des twigger
$twig_reference = new XML::Twig;
$twig_acomparer = new XML::Twig;
$twig_output = new XML::Twig;

#	initialisation du fichier de sortie par la routine init_output_file
init_output_file();

#	ex�cution de la routine principale pour effectuer le diff
diff_action();

#	d�but de la routine principale
sub diff_action
{	####	BLOC BRAND
	#	pr�paration de l'environnement des variables en m�moire
	#	initialisation du twig sur le fichier de r�f�rence
	$twig_reference -> parsefile($reference_file);
	$twig_reference_root = $twig_reference -> root;
	my $twig_reference_brand = $twig_reference_root -> first_child('brand');
	
	#	initialisation des twigs sur le fichier � comparer
	$twig_acomparer -> parsefile($acomparer_file);
	$twig_acomparer_root = $twig_acomparer -> root;
	my $twig_acomparer_brand = $twig_acomparer_root -> first_child('brand');
	my $twig_acomparer_make = $twig_acomparer_brand -> first_child('make');
	
	#	initialisation des twigs sur le fichier de sortie.
	$twig_output_root_new = $twig_output_initiator -> first_child('NEW');
	$twig_output_root_new -> insert_new_elt('brand');
	my $twig_output_root_brand = $twig_output_root_new -> first_child('brand');
	
	do
	{
		#	on r�cup�re les attributs de la marque
		my $acomparer_make_name = $twig_acomparer_make -> att('n');
		my $i = $twig_acomparer_make -> att('i');
		
		#	requ�te xpath pour voir si la marque existe
		my @tab_make_name = $twig_reference_root -> get_xpath("brand/make[\@n='$acomparer_make_name']");
		
		#	SI le tableau est vide...
		if( ! @tab_make_name)
		{
			#	on ajoute la marque dans le fichier de sortie.
			add_element("NEW", \$twig_output_root_brand, $twig_acomparer_make -> toString);
		}
		#	SINON on descend dans l'arbre vers le noeud root/brand/make/veh
		else #### BLOC VEHICULE
		{
			#	initialisation du twig sur le fichier de r�f�rence
			my $twig_reference_make = $twig_reference_brand -> first_child('make');
			
			#	initialisation des twigs sur le fichier � comparer
			my $twig_acomparer_veh = $twig_acomparer_make -> first_child('veh');
			
			#	initialisation des twigs sur le fichier de sortie.
			my $twig_output_make = $twig_output_root_brand -> insert_new_elt('make');
			$twig_output_make -> set_att('i' => $i, 'n' => $acomparer_make_name);
			
			do
			{
				#	on r�cup�re les attributs du v�hicule.
				my $acomparer_veh_name = $twig_acomparer_veh -> att('n');
				$i = $twig_acomparer_veh -> att('i');
				
				#	requ�te xpath pour voir si le v�hicule existe 
				my @tab_veh_name = $twig_reference_root -> get_xpath("brand/make/veh[\@n='$acomparer_veh_name']");
				
				#	SI le tableau est vide...
				if( ! @tab_veh_name)
				{
					#	on ajoute le v�hicule dans le fichier de sortie.
					add_element("NEW", \$twig_output_make,  $twig_acomparer_veh -> toString );
				}
				#	SINON on descend dans l'arbre vers le noeud root/brand/make/veh/mod
				else ####	BLOC MODELE
				{
				
					#	initialisation du twig sur le fichier de r�f�rence
					my $twig_reference_veh = $twig_reference_make -> first_child('veh');
					
					#	initialisation des twigs sur le fichier � comparer
					my $twig_acomparer_mod = $twig_acomparer_veh -> first_child('mod');
					
					do
					{
						#	on r�cup�re les attributs du mod�le.
						my $acomparer_mod_name = $twig_acomparer_mod -> att('n');
						$i = $twig_acomparer_mod -> att('i');
						
						#	requ�te xpath pour voir si le mod existe
						my @tab_mod_name = $twig_reference_root -> get_xpath("brand/make/veh/mod[\@n='$acomparer_mod_name']");
						
						#	SI le tableau est vide...
						if( ! @tab_mod_name)
						{
							#	on ajoute le mod�le dans le fichier de sortie.
							#	initialisation des twigs sur le fichier de sortie.
							my $twig_output_veh = $twig_output_make -> insert_new_elt('veh');
							$twig_output_veh -> set_att('i' => $i, 'n' => $acomparer_veh_name);
							add_element("NEW", \$twig_output_veh, $twig_acomparer_mod -> toString)
						}
						else ####	BLOC FAMILLE
						{
							#	initialisation du twig sur le fichier de r�f�rence
							my $twig_reference_mod = $twig_reference_veh -> first_child('mod');
							
							#	initialisation des twigs sur le fichier � comparer
							my $twig_acomparer_fam = $twig_acomparer_mod -> first_child('fam');
							
							do
							{
								#	on r�cup�re les attributs de fam
								my $acomparer_fam_name = $twig_acomparer_fam -> att('n');
								$i =  $twig_acomparer_fam -> att('i');
								
								#	requ�te xpath pour voir si le fam existe
								my @tab_fam_name = $twig_reference_root -> get_xpath("brand/make/veh/mod/fam[\@n='$acomparer_fam_name']");
								
								#	initialisation des twigs sur le fichier de sortie.
									my $twig_output_mod;
								
								#	SI le tableau est vide...
								if( ! @tab_fam_name)
								{
									#	on ajoute la famille dans le fichier de sortie.
									$twig_output_mod = $twig_output_veh -> insert_new_elt('mod');
									$twig_output_mod -> set_att('i' => $i, 'n' => $acomparer_mod_name);
									add_element("NEW", \$twig_output_mod, $twig_acomparer_fam -> toString);
								}
								else
								{
									#	initialisation du twig sur le fichier de r�f�rence
									my $twig_reference_fam = $twig_reference_mod -> first_child('fam');
									
									#	initialisation des twigs sur le fichier � comparer
									my $twig_acomparer_sys = $twig_acomparer_fam -> first_child('sys');
									
									do
									{
										#	on r�cup�re les attributs de sys
										my $acomparer_sys_name = $twig_acomparer_sys -> att('n');
										
										$i = $twig_acomparer_sys -> att('i');
										
										#	requ�te xpath pour voir si le sys existe
										my @tab_sys_name = $twig_reference_root -> get_xpath("brand/make/veh/mod/fam/sys[\@n='$acomparer_sys_name']");
										
										#	initialisation des twigs sur le fichier de sortie.
											my $twig_output_fam;
										
										#	SI le tableau est vide...
										if( ! @tab_sys_name)
										{
											print "acomparer_sys_name" . $acomparer_sys_name ."\n";
											#	on ajoute le sys dans le fichier de sortie.
											$twig_output_fam = $twig_output_mod -> insert_new_elt('fam');
											$twig_output_fam -> set_att('i' => $i, 'n' => $acomparer_fam_name);
											add_element("NEW", \$twig_output_fam, $twig_acomparer_sys -> toString);
										}
										else
										{
											
										}
									}
									while($twig_acomparer_sys = $twig_acomparer_sys -> sibling(1))
								}
							}
							while($twig_acomparer_fam = $twig_acomparer_fam -> sibling(1))
						}
					}
					while($twig_acomparer_mod = $twig_acomparer_mod -> sibling(1))
				}
			}
			while($twig_acomparer_veh = $twig_acomparer_veh -> sibling(1))
		}
	}
	while($twig_acomparer_make = $twig_acomparer_make -> sibling(1))
	
}

sub add_element
{
	my ($noeud, $twig_parent, $element) = @_;
	switch($noeud)
	{
		case "NEW"
		{
			$$twig_parent -> insert_new_elt($element);
		}
		case "DELETED"
		{
			$$twig_output_deleted -> insert_new_elt($element);
		}
		case "MODIFIED"
		{
			$$twig_output_modified -> insert_new_elt($element);
		}
		else
		{
			print "La soutine add_element ne peut pas s'ex�cuter car $noeud est un noeud non existant. \n"
		}
	}
	$twig_output_initiator -> print_to_file($output_file);
}

sub init_output_file
{
	$twig_output_initiator = new XML::Twig::Elt -> new ("root_a_determiner");
	$twig_output_initiator -> set_pretty_print('indented');
	
	$twig_output_initiator -> insert_new_elt("NEW");
	$twig_output_initiator -> insert_new_elt("DELETED");
	$twig_output_initiator -> insert_new_elt("MODIFIED");
	
	$twig_output_initiator -> print_to_file($output_file);	
}