#==============================================================================
# VCM.pl
# Vims Check Make
# diff xls and csv vims make file
# parameters
#     0: csv vims make file
#     1: xml vims make file
#
#==============================================================================
use strict;
use XML::Twig;

use constant DEBUG=>0;
my $Version="0.1A";

if ( @ARGV!=2 ) {
    die "usage: $0 <csv vims make file> <xml vims make file>\n";
    }

# compute path file name
my $Inputcsv= $ARGV[0];
my $Inputxml= $ARGV[1];

print "VCM:begin GMT ".gmtime()."\n";

# verify input files can be opened
if ( not open(INPUTXML,"<$Inputxml") ) {
    die "unable to open file $Inputxml";
    }
close (INPUTXML);
if ( not open(INPUTCSV,"<$Inputcsv") ) {
    die "unable to open file $Inputcsv";
    }
#..............................................................................
my %NameListXML;
my %NameListCSV;
#..............................................................................
# parse xml files
my $twig= new XML::Twig (
       PrettyPrint   => 'indented_c' ,
       keep_encoding => 1 ,
       twig_handlers => 
          { 'TIPO'  => \&elttipo,
          }
       );
if (DEBUG) { print "XML\n"; }
$twig->parsefile( $Inputxml );
#..............................................................................
# parse csv files
my $CurrentLine;
# current line number beeing parsed
my $LineNumber=0;
# csv fields
my @Fields;
# Field names ordered by ordinal number
my @FieldName;
@FieldName=( 'id','name' );
my $FieldNumbers=2;
# indirection table to find position of a field name
my %FieldPos;
# constructs FieldPos Hash indirection table
my $i;
for ( $i=0; $i<$FieldNumbers ; $i++ ) {
    $FieldPos{ $FieldName[$i] }=$i;
    }

if (DEBUG) { print "CSV\n"; }

while ( $CurrentLine=<INPUTCSV> ) {
	#
    $LineNumber++;

    # get number of fields
    my @separators=($CurrentLine=~m/;/g);
    my $CurFieldNumbers=@separators+1;
    # clean trailing \n
    chomp $CurrentLine;
    # get all the fields 
    @Fields=split(/;/, $CurrentLine);	
    if (DEBUG) { printf "code:%s make:%s\n",$Fields[$FieldPos {'id'}],$Fields[$FieldPos {'name'}]; }

    if ( exists $NameListCSV{ $Fields[$FieldPos {'name'}] } ) {
    	# error
    	printf "CSV DOUBLON code:%s make:%s\n",$Fields[$FieldPos {'id'}],$Fields[$FieldPos {'name'}];
        }
    else {
	    $NameListCSV{ $Fields[$FieldPos {'name'}] }= $Fields[$FieldPos {'id'}];
        }
    }
#..............................................................................
# compare Make lists

my @xmlmk=sort keys(%NameListXML);
foreach my $m (@xmlmk) {
	#
    if ( exists $NameListCSV{ $m } ) {
    	# match !
    	# comparaison des ID ?
    	delete $NameListCSV{ $m };
    	delete $NameListXML{ $m };
        }
	}
# check xmlmk
print "\nXML NEW MAKE\n".('='x15)."\n";
@xmlmk=sort keys(%NameListXML);
foreach my $m (@xmlmk) {
    printf "NEW %s\n", $m;
}
# check csvmk
print "\nCSV NOT MENTIONNED MAKE\n".('='x15)."\n";
my @csvmk=sort keys(%NameListCSV);
foreach my $m (@csvmk) {
    printf "OLD %s\n", $m;
}

print "VCM:end GMT ".gmtime()."\n";
#------------------------------------------------------------------------------
# parse twig
sub elttipo
{
my( $t, $elt)= @_;
my $par=$elt->parent();
if (($par->name eq 'CATEGORIA') && ($par->att('NOMBRE') eq 'MARCA')) {
    # parent element of TIPO is MARCA
    if (DEBUG) { printf "code:%d make:%s\n",$elt->att('COD_TIPO'),$elt->att('NOMBRE'); }

    if ( exists $NameListXML{ $elt->att('NOMBRE') } ) {
    	# error
    	printf "XML DOUBLON code:%s make:%s\n",$elt->att('COD_TIPO'),$elt->att('NOMBRE');
        }
    else {
	    $NameListXML{ $elt->att('NOMBRE') }= $elt->att('COD_TIPO');
        }

    }

}
#======================== END OF FILE =========================================