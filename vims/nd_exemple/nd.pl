#==============================================================================
# nd.pl
# nd : Nav Diff
# make diff between two courbe de r�f�rence navigation file
#
# nd.pl <file.xml> <reference file.xml> <result file.xml>
#  1: file : file to compare
#  2: reference file : reference file for comparison
#
#=============================================================================
use strict;
use XML::Twig;
use POSIX qw(asctime);

use constant DEBUG=>1;
use constant VERSION=>"0.1A";
use constant DIFFMARK=>'!='; 

my $file_make_elt; my @ref_make_elt; my $make_name; my $twig_make; my $ref_rest_make_elt;
my $file_veh_elt;  my @ref_veh_elt;  my $veh_name;  my $twig_veh;  my $ref_rest_veh_elt;
my $file_mod_elt;  my @ref_mod_elt;  my $mod_name;  my $twig_mod;  my $ref_rest_mod_elt;
my $file_fam_elt;  my @ref_fam_elt;  my $fam_name;  my $twig_fam;  my $ref_rest_fam_elt;
my $file_sys_elt;  my @ref_sys_elt;  my $sys_name;  my $twig_sys;  my $ref_rest_sys_elt;
my $file_eng_elt;  my @ref_eng_elt;  my $eng_name;  my $twig_eng;  my $ref_rest_eng_elt;
my $file_crv_elt;  my @ref_crv_elt;  my $crv_name;  my $twig_crv;  my $ref_rest_crv_elt; my $crv_id;


# complain if there aren't enough args
if ( $#ARGV != 2 )
    { 
    print "Usage: $0 <file1> <file2> <result file>\n";
    die "\tIncorrect args found: $#ARGV";
    }

my $file=shift;
my $Ref_file=shift;
my $OutputFile=shift;
#..............................................................................
my $twig; my $xml_new; my $xml_deleted; my $xml_modified;
&InitInfoFile($OutputFile,\$twig,\$xml_new,\$xml_deleted,\$xml_modified,$file,$Ref_file);

#..............................................................................
&Diff_file($file,$Ref_file);


# create result xml file
open (OUTPUTFILE, ">$OutputFile");
$twig->flush(\*OUTPUTFILE);
close (OUTPUTFILE);

#------------------------------------------------------------------------------
# Diff_file
#
# Parameters
#  1: $file : file to compare
#  2: $Ref_file : reference file for comparison
#------------------------------------------------------------------------------
sub Diff_file
{
my $file; my $Ref_file;
($file,$Ref_file) = @_;

#..............................................................................
my $file_twig, my $ref_twig;
my $file_root, my $ref_root;
my $file_FileNumber;
my $file_elt;
my $file_brand_elt;
my $new_make;

$file_twig= new XML::Twig ();
$file_twig->parsefile( $file );
$file_root= $file_twig->root;
$file_brand_elt=$file_root->first_child('brand');

$ref_twig= new XML::Twig ();
$ref_twig->parsefile( $Ref_file );
$ref_root= $ref_twig->root;

while ($file_make_elt=$file_brand_elt->first_child('make')) {
    # tant qu'il y a des marques dans le fichier test�
    $make_name=$file_make_elt->att('n');

    # cherche la marque trouv�e dans le fichier de reference
    @ref_make_elt=$ref_root->get_xpath ( "brand/make[\@n='$make_name']" );

    if (! @ref_make_elt) {
        # la marque n'existe pas dans la r�f�rence
        # alors c'est NEW !
        $file_make_elt->cut;
        $file_make_elt->paste( 'last_child', $xml_new);
        }
    else {
        # la marque existe dans la r�f�rence : on continue la descente dans l'arbre

        while ($file_veh_elt=$file_make_elt->first_child('veh')) {
            # tant qu'il y a des vehicules dans la marque en cours du fichier test�
            $veh_name=$file_veh_elt->att('n');

            # cherche le v�hicule trouv� dans le fichier de reference
            @ref_veh_elt=$ref_make_elt[0]->get_xpath ( "veh[\@n='$veh_name']" );
            
            if (! @ref_veh_elt) {
                # le v�hicule n'existe pas dans la r�f�rence
                # alors c'est NEW !

                &Get_twig_make(\$xml_new);
                $file_veh_elt->cut;
                $file_veh_elt->paste( 'last_child', $twig_make);
                }
            else {
                # le v�hicule existe dans la r�f�rence : on continue la descente dans l'arbre
                
                while ($file_mod_elt=$file_veh_elt->first_child('mod')) {
                    # tant qu'il y a des mod�les dans le v�hicule en cours du fichier test�
                    $mod_name=$file_mod_elt->att('n');
                    
                    # cherche le mod�le trouv� dans le fichier de reference
                    @ref_mod_elt=$ref_veh_elt[0]->get_xpath ( "mod[\@n='$mod_name']" );

                    if (! @ref_mod_elt) {
                        # le mod�le n'existe pas dans la r�f�rence
                        # alors c'est NEW !

                        &Get_twig_veh(\$xml_new);
                        $file_mod_elt->cut;
                        $file_mod_elt->paste( 'last_child', $twig_veh);
                        }
                    else {
                        # le mod�le existe dans la r�f�rence : on continue la descente dans l'arbre
                        
                        while ($file_fam_elt=$file_mod_elt->first_child('fam')) {
                            # tant qu'il y a des familles dans le mod�le en cours du fichier test�
                            $fam_name=$file_fam_elt->att('n');
                            
                            # cherche la famille trouv�e dans le fichier de reference
                            @ref_fam_elt=$ref_mod_elt[0]->get_xpath ( "fam[\@n='$fam_name']" );
                            
                            if (! @ref_fam_elt) {
                                # la famille n'existe pas dans la r�f�rence
                                # alors c'est NEW !
                                
                                &Get_twig_mod(\$xml_new);
                                $file_fam_elt->cut;
                                $file_fam_elt->paste( 'last_child', $twig_mod);
                                }
                            else {
                                # la famille existe dans la r�f�rence : on continue la descente dans l'arbre

                                while ($file_sys_elt=$file_fam_elt->first_child('sys')) {
                                    # tant qu'il y a des syst�mes dans la famille en cours du fichier test�
                                    $sys_name=$file_sys_elt->att('n');
                                    
                                    # cherche le syst�me trouv� dans le fichier de reference
                                    @ref_sys_elt=$ref_fam_elt[0]->get_xpath ( "sys[\@n='$sys_name']" );
                                	
                                	if (! @ref_sys_elt) {
                                	    # le syst�me n'existe pas dans la r�f�rence
                                        # alors c'est NEW !
                                        
                                        &Get_twig_fam(\$xml_new);
                                        $file_sys_elt->cut;
                                        $file_sys_elt->paste( 'last_child', $twig_fam);
                                	    }
                                	else {
                                	    # le syst�me existe dans la r�f�rence : on continue la descente dans l'arbre

                                        while ($file_eng_elt=$file_sys_elt->first_child('eng')) {
                                            # tant qu'il y a des moteurs dans le syst�me en cours du fichier test�
                                            
                                            $eng_name=$file_eng_elt->att('n');
                                            
                                            # cherche le moteur trouv� dans le fichier de reference
                                            @ref_eng_elt=$ref_sys_elt[0]->get_xpath ( "eng[\@n='$eng_name']" );
                                            
                                            if (! @ref_eng_elt) {
                                                # le moteur n'existe pas dans la r�f�rence
                                                # alors c'est NEW !
                                                
                                                &Get_twig_sys(\$xml_new);
                                                $file_eng_elt->cut;
                                                $file_eng_elt->paste( 'last_child', $twig_sys);
                                                }
                                            else {
                                            	# le moteur existe dans la r�f�rence : on continue la descente dans l'arbre

                                                while ($file_crv_elt=$file_eng_elt->first_child('crv')) {
                                                    # tant qu'il y a des courbes dans le moteur en cours du fichier test�
                                                    # attention, n�cessite que <crvlabel> existe et non vide
                                                
                                                    $crv_name=$file_crv_elt->first_child('crvlabel')->text;
                                                    # l'id est beaucoup plus fiable que le nom, surtout en cas d'homonymes de courbes et si l'ordre n'est pas respect�
                                                    $crv_id=$file_crv_elt->att('i');
                                                    
                                                    #if ($crv_id==20384) {
                                                        #
                                                        #print "debug:$crv_id \n";
                                                        #}
                                                    
                                                    # cherche la courbe trouv�e dans le fichier de reference, on passe maintenant par l'id plut�t que par le nom
                                                    #@ref_crv_elt=$ref_eng_elt[0]->get_xpath ( "crv/crvlabel[text()=\'$crv_name\']" );
                                                    @ref_crv_elt=$ref_eng_elt[0]->get_xpath ( "crv[\@i='$crv_id']");

                                                    if (! @ref_crv_elt) {
                                                        # la courbe n'existe pas dans la r�f�rence
                                                        # alors c'est NEW !
                                                        
                                                        &Get_twig_eng(\$xml_new);
                                                        $file_crv_elt->cut;
                                                        $file_crv_elt->paste( 'last_child', $twig_eng);
                                                        }
                                                    else {
                                                        # la courbe existe dans la r�f�rence
                                                        # modified ?

                                                        my $crvdiff=&Cmp_twig_crv(\$file_crv_elt,\$ref_crv_elt[0]);
                                                        if ($crvdiff) {
                                                            # les courbes sont diff�rentes donc la r�f�rence a �t� modifi�e
                                                            
                                                            &Get_twig_eng(\$xml_modified);
                                                            $file_crv_elt->cut;
                                                            $file_crv_elt->paste( 'last_child', $twig_eng);
                                                            }
                                                        else {
                                                        	# les courbes sont identiques donc on supprime la courbe trouv�e
                                                            $file_crv_elt->delete;
                                                            }
                                                            
                                                        # supprime une courbe trouv�e dans la r�f�rence (optimise les recherches suivantes)
                                                        $ref_crv_elt[0]->delete;
                                                        }
                                                    }
                                                
                                                # reste-t-il des courbes pour ce moteur dans la r�f�rence ?
                                                while ($ref_rest_crv_elt=$ref_eng_elt[0]->first_child('crv')) {
                                                    # tant qu'il reste des courbes pour ce moteur dans la r�f�rence => DELETED
                                                    &Get_twig_eng(\$xml_deleted);
                                                    $ref_rest_crv_elt->cut;
                                                    $ref_rest_crv_elt->paste( 'last_child', $twig_eng);
                                                    }
                                                
                                                $file_eng_elt->delete;   #en cours de dev, � enlever plus tard
                                                $ref_eng_elt[0]->delete;    # supprime un moteur trouv� dans la r�f�rence
                                                }
                                            }
                                        
                                        # reste-t-il des moteurs pour ce syst�me dans la r�f�rence ?
                                        while ($ref_rest_eng_elt=$ref_sys_elt[0]->first_child('eng')) {
                                            # tant qu'il reste des moteurs pour ce syst�me dans la r�f�rence => DELETED
                                            &Get_twig_sys(\$xml_deleted);
                                            $ref_rest_eng_elt->cut;
                                            $ref_rest_eng_elt->paste( 'last_child', $twig_sys);
                                            }
                                        
                                        $file_sys_elt->delete;   #en cours de dev, � enlever plus tard
                                        $ref_sys_elt[0]->delete;    # supprime un syst�me trouv� dans la r�f�rence
                                	    }
                                    }
                                
                                # reste-t-il des syst�mes pour cette famille dans la r�f�rence ?
                                while ($ref_rest_sys_elt=$ref_fam_elt[0]->first_child('sys')) {
                                    # tant qu'il reste des syst�mes pour cette famille dans la r�f�rence => DELETED
                                    &Get_twig_fam(\$xml_deleted);
                                    $ref_rest_sys_elt->cut;
                                    $ref_rest_sys_elt->paste( 'last_child', $twig_fam);
                                    }
                                
                                $file_fam_elt->delete;   #en cours de dev, � enlever plus tard
                                $ref_fam_elt[0]->delete;    # supprime une famille trouv�e dans la r�f�rence
                                }
                            }
                        
                        # reste-t-il des familles pour ce mod�le dans la r�f�rence ?
                        while ($ref_rest_fam_elt=$ref_mod_elt[0]->first_child('fam')) {
                            # tant qu'il reste des familles pour ce mod�le dans la r�f�rence => DELETED
                            &Get_twig_mod(\$xml_deleted);
                            $ref_rest_fam_elt->cut;
                            $ref_rest_fam_elt->paste( 'last_child', $twig_mod);
                            }
                        
                        $file_mod_elt->delete;   #en cours de dev, � enlever plus tard  : quand MODIFIED sera trait�
                        $ref_mod_elt[0]->delete;    # supprime un mod�le trouve dans la r�f�rence
                        }
                    }
                
                # reste-t-il des mod�les pour ce v�hicule dans la r�f�rence ?
                while ($ref_rest_mod_elt=$ref_veh_elt[0]->first_child('mod')) {
                    # tant qu'il reste des mod�les pour ce v�hicule dans la r�f�rence => DELETED
                    &Get_twig_veh(\$xml_deleted);
                    $ref_rest_mod_elt->cut;
                    $ref_rest_mod_elt->paste( 'last_child', $twig_veh);
                    }
                
                $file_veh_elt->delete;   #en cours de dev, � enlever plus tard  : quand MODIFIED sera trait�
                $ref_veh_elt[0]->delete;    # supprime un v�hicule trouve dans la r�f�rence
                }
            }

        # reste-t-il des v�hicules pour cette marque dans la r�f�rence ?
        while ($ref_rest_veh_elt=$ref_make_elt[0]->first_child('veh')) {
            # tant qu'il reste des vehicules pour cette marque dans la r�f�rence => DELETED
            &Get_twig_make(\$xml_deleted);
            $ref_rest_veh_elt->cut;
            $ref_rest_veh_elt->paste( 'last_child', $twig_make);
            }
        
        $file_make_elt->delete;   #en cours de dev, � enlever plus tard : quand MODIFIED sera trait�
        $ref_make_elt[0]->delete;    # une marque trouv�e dans la r�f�rence est trait�e
        }
    }
# recherche dans le fichier de r�f�rence les marques qui restent => DELETED
my $ref_brand_elt=$ref_root->first_child('brand');
while ($ref_rest_make_elt=$ref_brand_elt->first_child('make')) {
    # tant qu'il reste des marques dans la r�f�rence
    $ref_rest_make_elt->cut;
    $ref_rest_make_elt->paste( 'last_child', $xml_deleted);
    }


# NB: il y a une opportunit� d'am�lioration
# on pourait reparser toutes les courbes des NEW et DELETED pour voir
# si des v�hicules n'ont pas �t� renomm�s

}
#------------------------------------------------------------------------------
# InitInfoFile
# Create empty xml output file
# Parameters
#  1: $OutputFile : name of file for output
#  2: $twig : reference to twig from $OutputFile
#  3: $xml_new : reference to $xml_new of twig : new file
#  4: $xml_erased : reference to $xml_erased of twig : erased file
#  5: $xml_modified : reference to $xml_modified of twig : modified file
#------------------------------------------------------------------------------
sub InitInfoFile
{
my $OutputFile; my $twig; my $xml_new; my $xml_deleted; my $xml_modified; my $file; my $Ref_file;
($OutputFile,$twig,$xml_new,$xml_deleted,$xml_modified,$file,$Ref_file) = @_;

# create empty xml file
if ( not open (OUTPUTFILE, ">$OutputFile") ) {
    die "unable to open file $OutputFile";
}
my $gmtime='GMT '.gmtime();
my $Version=VERSION;
print OUTPUTFILE <<EOF;
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE brand [
<!ENTITY % HTMLlat1 PUBLIC
        "-//W3C//ENTITIES Latin 1 for XHTML//EN"
        "./xhtml-lat1.ent">
%HTMLlat1;

<!ENTITY % HTMLspecial PUBLIC
        "-//W3C//ENTITIES Special for XHTML//EN"
        "./xhtml-special.ent">
%HTMLspecial;

<!ENTITY % HTMLsymbol PUBLIC
        "-//W3C//ENTITIES Symbols for XHTML//EN"
        "./xhtml-symbol.ent">
%HTMLsymbol;
]>
<!-- This file is COPYRIGHT by FTQ. It is automatically generated by nd -->
<ND Date="$gmtime" GeneratorVersion="$Version">
<NEW/>
<DELETED/>
<MODIFIED/>
</ND>
EOF
close (OUTPUTFILE);
#..............................................................................
# create twig
$$twig= new XML::Twig ( PrettyPrint=>'indented_c' ,
                        keep_encoding=>1 );
$$twig->parsefile( $OutputFile );
my $xml_root=$$twig->root;
$xml_root->set_att(
        'NewFile'   => $file,
        'BaseFile'  => $Ref_file
        );
$$xml_new=$xml_root->first_child("NEW");
$$xml_deleted=$xml_root->first_child("DELETED");
$$xml_modified=$xml_root->first_child("MODIFIED");
}
#------------------------------------------------------------------------------
# Get_twig_make
# recherche d'une marque dans un twig (NEW, DELETED, MODIFIED), la cr�e si elle n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $make_name
#  in  $file_make_elt
#  out $twig_make
#------------------------------------------------------------------------------
sub Get_twig_make
{
my $twig_elt;
my @make_elt;

($twig_elt) = @_;

@make_elt=$$twig_elt->get_xpath ( "make[\@n='$make_name']" );

if (@make_elt)
    {
    # la marque existe
    $twig_make=$make_elt[0];
    }
else
    {
    # la marque n'existe pas dans le twig, on la cr�e
    $twig_make=new XML::Twig::Elt( 'make' );
    $twig_make->set_att(
                 'n' => $make_name,
                 'i' => $file_make_elt->att('i'),
                 );
    $twig_make->paste( 'last_child', $$twig_elt);
    }
}
#------------------------------------------------------------------------------
# Get_twig_veh
# recherche d'un v�hicule dans un twig (NEW, DELETED, MODIFIED), le cr�e s'il n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $veh_name
#  in  $file_veh_elt
#  out $twig_veh
#------------------------------------------------------------------------------
sub Get_twig_veh
{
my $twig_elt;
my @veh_elt;

($twig_elt) = @_;

&Get_twig_make($twig_elt);
@veh_elt=$twig_make->get_xpath ( "veh[\@n='$veh_name']" );

if (@veh_elt)
    {
    # le v�hicule existe
    $twig_veh=$veh_elt[0];
    }
else
    {
    # le v�hicule n'existe pas dans le twig, on le cr�e
    $twig_veh=new XML::Twig::Elt( 'veh' );
    $twig_veh->set_att(
                 'n' => $veh_name,
                 'i' => $file_veh_elt->att('i'),
                 );
    $twig_veh->paste( 'last_child', $twig_make);
    }
}
#------------------------------------------------------------------------------
# Get_twig_mod
# recherche du mod�le dans un twig (NEW, DELETED, MODIFIED), le cr�� s'il n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $mod_name
#  in  $file_mod_elt
#  out $twig_mod
#------------------------------------------------------------------------------
sub Get_twig_mod
{
my $twig_elt;
my @mod_elt;

($twig_elt) = @_;

&Get_twig_veh($twig_elt);
@mod_elt=$twig_veh->get_xpath ( "mod[\@n='$mod_name']" );

if (@mod_elt)
    {
    # le mod�le existe
    $twig_mod=$mod_elt[0];
    }
else
    {
    # le mod�le n'existe pas dans le twig, on le cr�e
    $twig_mod=new XML::Twig::Elt( 'mod' );
    $twig_mod->set_att(
                 'n' => $mod_name,
                 'i' => $file_mod_elt->att('i'),
                 );
    $twig_mod->paste( 'last_child', $twig_veh);
    }
}
#------------------------------------------------------------------------------
# Get_twig_fam
# recherche de la famille dans un twig (NEW, DELETED, MODIFIED), la cr�� si elle n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $fam_name
#  in  $file_fam_elt
#  out $twig_fam
#------------------------------------------------------------------------------
sub Get_twig_fam
{
my $twig_elt;
my @fam_elt;

($twig_elt) = @_;

&Get_twig_mod($twig_elt);
@fam_elt=$twig_mod->get_xpath ( "fam[\@n='$fam_name']" );

if (@fam_elt)
    {
    # la famille existe
    $twig_fam=$fam_elt[0];
    }
else
    {
    # la famille n'existe pas dans le twig, on la cr�e
    $twig_fam=new XML::Twig::Elt( 'fam' );
    $twig_fam->set_att(
                 'n' => $fam_name,
                 'i' => $file_fam_elt->att('i'),
                 );
    $twig_fam->paste( 'last_child', $twig_mod);
    }	
}
#------------------------------------------------------------------------------
# Get_twig_sys
# recherche du syst�me dans un twig (NEW, DELETED, MODIFIED), le cr�� s'il n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $sys_name
#  in  $file_sys_elt
#  out $twig_sys
#------------------------------------------------------------------------------
sub Get_twig_sys
{
my $twig_elt;
my @sys_elt;

($twig_elt) = @_;

&Get_twig_fam($twig_elt);
@sys_elt=$twig_fam->get_xpath ( "sys[\@n='$sys_name']" );

if (@sys_elt)
    {
    # le syst�me existe
    $twig_sys=$sys_elt[0];
    }
else
    {
    # le syst�me n'existe pas dans le twig, on le cr�e
    $twig_sys=new XML::Twig::Elt( 'sys' );
    $twig_sys->set_att(
                 'n' => $sys_name,
                 'i' => $file_sys_elt->att('i'),
                 );
    $twig_sys->paste( 'last_child', $twig_fam);
    }	
}
#------------------------------------------------------------------------------
# Get_twig_eng
# recherche du moteur dans un twig (NEW, DELETED, MODIFIED), le cr�� s'il n'existe pas
# Parameters
#  1: �l�ment NEW
#
# Global
#  in  $eng_name
#  in  $file_eng_elt
#  out $twig_eng
#------------------------------------------------------------------------------
sub Get_twig_eng
{
my $twig_elt;
my @eng_elt;

($twig_elt) = @_;

&Get_twig_sys($twig_elt);
@eng_elt=$twig_sys->get_xpath ( "eng[\@n='$eng_name']"  );

if (@eng_elt)
    {
    # le moteur existe
    $twig_eng=$eng_elt[0];
    }
else
    {
    # le moteur n'existe pas dans le twig, on le cr�e
    $twig_eng=new XML::Twig::Elt( 'eng' );
    $twig_eng->set_att(
                 'n' => $eng_name,
                 'i' => $file_eng_elt->att('i'),
                 );
    $twig_eng->paste( 'last_child', $twig_sys);
    }	
}
#------------------------------------------------------------------------------
# Cmp_twig_crv
# compare deux �l�ments courbes pour voir s'ils sont identiques
# retourne le r�sultat de comparaison et la liste des �l�ments qui diff�rent
# Parameters
#  1: $twig_file : �l�ment courbe du fichier � comparer
#  2: $twig_ref : �l�ment courbe du fichier de r�f�rence
#
#------------------------------------------------------------------------------
sub Cmp_twig_crv
{
my $twig_file;
my $twig_ref;

($twig_file,$twig_ref) = @_;

my @taglist = ('b', 'd', 'f', 'i', 'ms', 'p', 'td', 'tl', 'ts', 'v1d', 'v1o');
my $tag; my $i;
my $max=@taglist; my $cmp;
# comparaison globale =0 : les deux �l�ments sont identiques
my $glbcmp=0;
# liste des tags modifi�s
my @modtaglist=();

# comparaison des attribus
for ($i=0;$i<$max;$i++) {
    #
    $tag=$taglist[$i];
    $cmp =  ( $$twig_file->att($tag) ) ne ( $$twig_ref->att($tag) ) ;
    if ($cmp) {
        # les attributs ne sont pas �gaux
        # les �l�ments sont diff�rents
        $glbcmp=1;
        # ajoute le tag � la liste des modifi�s
        push @modtaglist, $tag;
        # marque le champ comme modifi�
        $$twig_file->set_att( $tag, $$twig_file->att($tag) . DIFFMARK);
        }
    }

# comparaison du fils tlbl
my $file_tlbl=$$twig_file->first_child('tlbl')->text;
my $ref_tlbl=$$twig_ref->first_child('tlbl')->text;
if ( $file_tlbl ne $ref_tlbl ) {
    # les tlbl sont diff�rents
    $glbcmp=1;
    push @modtaglist, 'tlbl';
    $$twig_file->first_child('tlbl')->set_text( $$twig_file->first_child('tlbl')->text . DIFFMARK);
    }

# comparaison du fils rlbl
my $file_rlbl=$$twig_file->first_child('rlbl')->text;
my $ref_rlbl=$$twig_ref->first_child('rlbl')->text;
if ( $file_rlbl ne $ref_rlbl ) {
    # les rlbl sont diff�rents
    $glbcmp=1;
    push @modtaglist, 'rlbl';
    $$twig_file->first_child('rlbl')->set_text( $$twig_file->first_child('rlbl')->text . DIFFMARK);
    }

return $glbcmp;
}
#  <crv b="REC" d="4109-T" f="apm1.asc" i="24735" ms="2" p="A1*2" td="-7" tl="4" ts="+" v1d="50" v1o="-3">
#    <crvlabel>@P474-POLUXMES</crvlabel>
#    <tlbl>@P775-POLUXMES@T.@\n@\+@P1662-POLUXMES@\*1500 tr/min@T.</tlbl>
#    <rlbl>@P504-POLUXMES@T.</rlbl>
#  </crv>
#------------------------------------------------------------------------------
# Cmp_twig_eng
# compare deux �l�ments engine pour voir s'ils sont identiques
# retourne le r�sultat de comparaison et la liste des �l�ments qui diff�rent
# Parameters
#  1: $twig_file : �l�ment engine du fichier � comparer
#  2: $twig_ref : �l�ment engine du fichier de r�f�rence
#
#------------------------------------------------------------------------------
sub Cmp_twig_eng
{
my $twig_file;
my $twig_ref;

($twig_file,$twig_ref) = @_;

my @taglist = ( 'i', 'n');
my $tag; my $i;
my $max=@taglist; my $cmp;
# comparaison globale =0 : les deux �l�ments sont identiques
my $glbcmp=0;
# liste des tags modifi�s
my @modtaglist=();

# comparaison des attribus
for ($i=0;$i<$max;$i++) {
    #
    $tag=$taglist[$i];
    $cmp =  ( $$twig_file->att($tag) ) ne ( $$twig_ref->att($tag) ) ;
    if ($cmp) {
        # les attributs ne sont pas �gaux
        # les �l�ments sont diff�rents
        $glbcmp=1;
        # ajoute le tag � la liste des modifi�s
        push @modtaglist, $tag;
        # marque le champ comme modifi�
        $$twig_file->set_att( $tag, $$twig_file->att($tag) . DIFFMARK);
        }
    }

return $glbcmp;
}
#              <eng i="486" n="HMZ">
#------------------------------------------------------------------------------
# Cmp_twig_sys
# compare deux �l�ments syst�me pour voir s'ils sont identiques
# retourne le r�sultat de comparaison et la liste des �l�ments qui diff�rent
# Parameters
#  1: $twig_file : �l�ment engine du fichier � comparer
#  2: $twig_ref : �l�ment engine du fichier de r�f�rence
#
#------------------------------------------------------------------------------
sub Cmp_twig_sys
{
my $twig_file;
my $twig_ref;

($twig_file,$twig_ref) = @_;

my @taglist = ( 'i', 'n');
my $tag; my $i;
my $max=@taglist; my $cmp;
# comparaison globale =0 : les deux �l�ments sont identiques
my $glbcmp=0;
# liste des tags modifi�s
my @modtaglist=();

# comparaison des attribus
for ($i=0;$i<$max;$i++) {
    #
    $tag=$taglist[$i];
    $cmp =  ( $$twig_file->att($tag) ) ne ( $$twig_ref->att($tag) ) ;
    if ($cmp) {
        # les attributs ne sont pas �gaux
        # les �l�ments sont diff�rents
        $glbcmp=1;
        # ajoute le tag � la liste des modifi�s
        push @modtaglist, $tag;
        # marque le champ comme modifi�
        $$twig_file->set_att( $tag, $$twig_file->att($tag) . DIFFMARK);
        }
    }

return $glbcmp;
}
#            <sys i="485" n="V46">
#------------------------------------------------------------------------------
# end.