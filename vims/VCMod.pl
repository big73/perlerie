#==============================================================================
# VCMod.pl
# Vims Check Model
# diff xls and csv vims model file
# parameters
#     0: csv vims model file
#     1: xls gouv model file
#
#==============================================================================
use strict;
use XML::Twig;

use constant DEBUG=>0;
my $Version="0.1A";

if ( @ARGV!=2 ) {
    die "usage: $0 <csv vims model file> <gouv vims model file>\n";
    }

# compute path file name
my $Inputcsv= $ARGV[0];
my $Inputxls= $ARGV[1];

print "VCMod:begin GMT ".gmtime()."\n";

# verify input files can be opened
if ( not open(INPUTXML,"<$Inputxls") ) {
    die "unable to open file $Inputxls";
    }
if ( not open(INPUTCSV,"<$Inputcsv") ) {
    die "unable to open file $Inputcsv";
    }
#..............................................................................
my %NameListXML;
my %NameListCSV;
#..............................................................................
# parse csv files
my $CurrentLine;
# current line number beeing parsed
my $LineNumber=0;
# csv fields
my @Fields;
# Field names ordered by ordinal number
my @FieldName;
@FieldName=( 'name','id' );
my $FieldNumbers=2;
# indirection table to find position of a field name
my %FieldPos;
# constructs FieldPos Hash indirection table
for ( my $i=0; $i<$FieldNumbers ; $i++ ) {
    $FieldPos{ $FieldName[$i] }=$i;
    }

if (DEBUG) { print "CSV\n"; }

while ( $CurrentLine=<INPUTCSV> ) {
	#
    $LineNumber++;

    # get number of fields
    my @separators=($CurrentLine=~m/;/g);
    my $CurFieldNumbers=@separators+1;
    # clean trailing \n
    chomp $CurrentLine;
    # get all the fields 
    @Fields=split(/;/, $CurrentLine);	
    if (DEBUG) { printf "code:|%s| model:|%s|\n",$Fields[$FieldPos {'id'}],$Fields[$FieldPos {'name'}]; }

    # regexp to get inner fields in record
    my @inner=split(/ - /, $Fields[$FieldPos {'name'}]);
    my $model=$inner[0];
    if (DEBUG) {
    	my $size=@inner;
    	printf "%d##",$size;
    	for (my $i=0; $i<$size; $i++) {
    	    printf "%s#",$inner[$i];
    	    }
    	print "\n";
    	}

    if ( exists $NameListCSV{ $model } ) {
    	# error
    	#printf "CSV DOUBLON code:|%s| model:|%s|\n",$Fields[$FieldPos {'id'}],$model;
        }
    else {
	    $NameListCSV{ $model }= $Fields[$FieldPos {'id'}];
        }
    }
close(INPUTCSV);
#..............................................................................
# parse xls files
if (DEBUG) { print "XLS\n"; }

# gouv dield names ordered by ordinal number
my @GouvName;
@GouvName=( 'make','model','transmission','cylinder','body','doors','other','type' );
my $GouvNumbers=8;
# indirection table to find position of a field name
my %GouvPos;
# constructs GouvPos Hash indirection table
for ( my $i=0; $i<$GouvNumbers ; $i++ ) {
    $GouvPos{ $GouvName[$i] }=$i;
    }

$LineNumber=0;
while ( $CurrentLine=<INPUTXML> ) {
	#
    $LineNumber++;

    # get number of fields
    my @separators=($CurrentLine=~m/;/g);
    my $CurFieldNumbers=@separators+1;
    # clean trailing \n
    chomp $CurrentLine;
    # get all the fields 
    @Fields=split(/;/, $CurrentLine);	

    if (DEBUG) {
    	print "gouv: ";
    	for (my $i=0; $i<$GouvNumbers; $i++) {
    	    printf "  %s:#%s#",$GouvName[$i],$Fields[$i];
    	    }
    	print "\n";
    	}

    if ( exists $NameListXML{ $Fields[ $GouvPos{'model'} ] } ) {
    	# error
    	# pas contributif : model n'est pas un identifiant : c'est la combinaison des champs qui identifient
    	#printf "CSV DOUBLON model:|%s|\n",$Fields[ $GouvPos{'model'} ];
        }
    else {
	    $NameListXML{ $Fields[ $GouvPos{'model'} ] }= $NameListXML{ $Fields[ $GouvPos{'make'} ] };
        }
    }
close (INPUTXML);
#..............................................................................
# compare model lists

my @csvmod=sort keys(%NameListCSV);
foreach my $m (@csvmod) {
	#
    if ( exists $NameListXML{ $m } ) {
    	# match !
    	delete $NameListCSV{ $m };
    	delete $NameListXML{ $m };
        }
	}

# check csvmod
print "\nVIMS NEW MODEL\n".('='x15)."\n";
@csvmod=sort keys(%NameListCSV);
foreach my $m (@csvmod) {
    printf "NEW %s\n", $m;
}
# check csvmk
print "\nGOUV NOT MENTIONNED MODEL\n".('='x15)."\n";
my @gouvmod=sort keys(%NameListXML);
foreach my $m (@gouvmod) {
    printf "NOT USED %s\n", $m;
}

print "VCMod:end GMT ".gmtime()."\n";
#======================== END OF FILE =========================================